'use strict'
const path = require('path')

module.exports = {
  entry: path.join(__dirname, 'src', 'index'), //aqr principal
  //path coloca a barra certo no caminho das pastas dependendo do sist operacional
  output: {
    path: path.join(__dirname, 'dist'), //dist eh a folder do bundle
    filename: 'bundle.js',
    publicPath: '/static/', //pasta do arq virtual em memoria
    //vai salvar o bundle em static
  },
  module: {
    loaders: [{
      test: /\.js$/, //todo aqr q termina com js
      exclude: /node_modules/, //menos na pasta node_modules
      include: /scr/, //de onde serao lidos os arq
      loader: 'babel-loader', //o babel vai ser o loader p compilar os arquivos .js de src
      options: {
        babelrc: true,
      }
    }]
  }
}