'use strict'
const path = require('path')

module.exports = {
  devtool: 'source-map',
  entry: path.join(__dirname, 'src', 'index'), //aqr principal
  //path coloca a barra certo no caminho das pastas dependendo do sist operacional
  output: {
    path: path.join(__dirname, 'dist'), //dist eh a folder do bundle
    filename: 'bundle.js',
    publicPath: '/dist/', //pasta do arq virtual em memoria
    //vai salvar o bundle em dist
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      include: /src/,
      loader: 'babel'
    }]
  }
}